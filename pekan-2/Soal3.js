//Fungsi cek nilai
function cek_nilai(a){
    
    if (a>80){
        return "A";
    }
    else if (a>70 && a<=80){
        return "B";
    }
    else if (a>60 && a<=70){
        return "C";
    }
    else if (a>50 && a<=60){
        return "D";
    }
    else if (a>=1 && a<=50){
        return "E";
    }
    else{
        return "-";
    }
}

const nilai = 100;
const hasil_nilai = cek_nilai(nilai);
console.log("Nilai Anda", nilai, "Mendapatkan Predikat",hasil_nilai);
import Form from './components/form/Form';
import CardForm from './components/form/CardForm';
import { useEffect, useState } from "react";
import './App.css';
import axios from "axios";

import './App.css';

function App() {

  const [products, setProducts] = useState([]);

  const fetchProducts = async () =>{
    try {
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/products"
      );
      setProducts(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <div> 
      <Form fetchProducts = {fetchProducts} />
      <CardForm 
        products={products}
        fetchProducts={fetchProducts}
        setProducts={setProducts}
      />
    </div>    
   );
}

export default App;

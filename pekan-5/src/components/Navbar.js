import React from 'react'

function Navbar() {
  return (
    <div>
      <header className="flex justify-around items-center bg-white py-3 shadow-lg">
      <div>
        <img src="./asset/logo.png" className="w-32" alt="" />
      </div>
      <div className="flex gap-6">
        <a href="./home.html">Home</a>
        <a href="./form.html">Form</a>
        <a href="./product.html">Products</a>
      </div>
      <div>
        <button className="bg-blue-400 ml-2 px-4 py-2 text-white">Login</button>
        <button className="bg-gray-100 ml-2 px-4 py-2">Register</button>
      </div>
    </header>
    </div>
  );
}

export default Navbar

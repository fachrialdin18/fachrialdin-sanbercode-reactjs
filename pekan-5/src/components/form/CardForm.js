import React from "react";

function CardForm( { products } ) {
    return( 
    <div>      
      <section className="mx-32">

      <div className="flex justify-between mb-3">
        <h1>Catalog Products</h1>
        <button className="bg-blue-500 text-white px-4 py-2">See more</button>
      </div>

      <div className="grid grid-cols-3 gap-8">
          
      {products.map((product) => {
        return (
          <div className="shadow-lg">
            <img
              src={product.image_url}
              className="w-full h-64 object-cover"
              alt=""
            />
            <div class="pl-4 my-4">
              <h1 className="text-blue-600 font-bold text-lg mb-3">{product.name}</h1>
              <p className="line-through text-red-600">{product.harga_display}</p>
              <p>{product.harga_diskon_display}</p>
              <p>Stock : {product.stock}</p>
            </div>
          </div>        
          );
      })}
      </div>

      </section>
    </div>

    


    );
}

export default CardForm;
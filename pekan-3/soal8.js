function sum(...numbers) {
    return numbers.reduce((total, current) => total + current, 0);
  }
  
  let result1 = sum(2, 4, 6);
  let result2 = sum(5, 2, 7, 4, 6, 8);
  
  console.log(result1); 
  console.log(result2); 
  
const arr = [5, 3, 4, 7, 6, 9, 2];

const newArray = arr.map((value, index) => {
  if (index % 2 === 0) {
    return value * 2;
  } else {
    return value * 3;
  }
});

console.log(newArray);

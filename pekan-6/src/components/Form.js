import axios from "axios";
import React, { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { ArticleContext } from "../context/ArticleContext";


function Form() {
  const { fetchArticles } = useContext(ArticleContext);
  const navigate = useNavigate();
  const [input, setInput] = useState({
    name: "",
    stock: "",
    harga: "",
    is_diskon: false,
    harga_diskon: "",
    category: "",
    image_url: "",
    description: "",
  });

  const handleChange = (e) => {
    if (e.target.name === "name") {
      setInput({ ...input, name: e.target.value });
    } else if (e.target.name === "stock") {
      setInput({ ...input, stock: e.target.value });
    } else if (e.target.name === "harga") {
        setInput({ ...input, harga: e.target.value });
    } else if (e.target.name === "is_diskon") {
        setInput({ ...input, is_diskon: e.target.checked });
    } else if (e.target.name === "harga_diskon") {
        setInput({ ...input, harga_diskon: e.target.value });
    } else if (e.target.name === "category") {
        setInput({ ...input, category: e.target.value });
    } else if (e.target.name === "image_url") {
      setInput({ ...input, image_url: e.target.value });
    } else if (e.target.name === "description") {
      setInput({ ...input, description: e.target.value }); // checkbox
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(input);
  };

  const resetForm = () => {
    setInput({
      name: "",
      stock: "",
      harga: "",
      is_diskon: false,
      harga_diskon: "",
      category: "",
      image_url: "",
      description: "",
    });
  };

  const storeArticle = async () => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/products",
        {
          name: input.name,
          stock: input.stock,
          harga: input.harga,
          is_diskon: input.is_diskon,
          harga_diskon: input.harga_diskon,
          category: input.category,
          image_url: input.image_url,
          description: input.description,
        }
      );
      alert("Berhasil Mengirim Request");
      // memannggil data kembali
      fetchArticles();
      resetForm();
      // navigasi ke halaman table
      navigate("/table");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  return (
    <div className="form">
    <h1 className="text-2xl font-bold text-center mb-6">Form Card</h1>
    <form>
        <div className="form-container-2a">
            <div className="form-section">
                <label for="nama-barang">Nama Barang</label>
                <input onChange={handleChange} value={input.name} type="text" name="name" placeholder="Masukkan Nama Barang" required />
            </div>
            <div className="form-section">
                <label for="stock-barang">Stock Barang</label>
                <input onChange={handleChange} value={input.stock} type="number" name="stock" placeholder="Masukkan Stock Barang" required />
            </div>
        </div>
        <div className="form-container-3">
            <div className="form-section">
                <label for="harga-barang">Harga Barang</label>
                <input onChange={handleChange} value={input.harga} type="number" step="0.01" name="harga" placeholder="Masukkan Harga Barang" required />
            </div>
            <div className="form-section">
                <label for="status-diskon">Status Diskon</label>
                <input onChange={handleChange} value={input.is_diskon} type="checkbox" name="is_diskon" />
            </div>
            <div className="form-section">
                <label for="harga-diskon">Harga Diskon</label>
                <input onChange={handleChange} value={input.harga_diskon} type="number" name="harga_diskon" placeholder="Masukkan Harga Diskon" required />
            </div>
        </div>
        <div className="form-container-2b">
            <div className="form-section">
                <label for="jenis-barang">Kategori Barang</label>
                <select onChange={handleChange} value={input.category} name="category" className="dropdown">
                    <option value="">Silahkan Pilih Kategori</option>
                    <option value="teknologi">Teknologi</option>
                    <option value="makanan">Makanan</option>
                    <option value="minuman">Minuman</option>
                    <option value="hiburan">Hiburan</option>
                    <option value="kendaraan">Kendaraan</option>
                </select>
            </div>
            <div className="form-section">
                <label for="image-barang">Image Barang</label>
                <input onChange={handleChange} value={input.image_url} type="text" name="image_url" placeholder="Masukkan Image Barang" required />
            </div>
        </div>
        <div className="form-container-1">
            <div className="form-section">
                <label for="Deskripsi">Deskripsi</label>
                <textarea onChange={handleChange} value={input.description} name="description" placeholder="Masukkan Deskripsi Barang" required  />
            </div>
        </div>
        <button onClick={storeArticle} className="bg-green-300 border px-6 py-2 mt-5">Create</button>
        <button type="submit" className="bg-red-300 border px-6 py-2 mt-5">Cancel</button>
    </form>
    </div>
  );
}

export default Form;

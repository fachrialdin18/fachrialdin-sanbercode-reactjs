import React from "react";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <header className="shadow-lg py-4 bg-white px-12 sticky top-0">
      <nav className="mx-auto max-w-7xl flex justify-between">
        <div>
          <img src="./asset/logo.png" className="w-32" alt="" />
        </div>
        <ul class="flex items-center gap-6 text-cyan-500 text-xl">
          <Link to="/">
            <li>Home</li>
          </Link>
          <Link to="/table">
            <li>Table</li>
          </Link>
        </ul>
        <div className="ml-3">
          <button className="bg-blue-400 ml-2 px-4 py-2 text-white mt-2">Login</button>
          <button className="bg-gray-100 ml-2 px-4 py-2 mt-2">Register</button>
        </div>
      </nav>
    </header>
  );
}

export default Navbar;


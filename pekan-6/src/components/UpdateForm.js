import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

function UpdateForm() {
  const { id } = useParams();
  const navigate = useNavigate();

  const [input, setInput] = useState({
    name: "",
    stock: "",
    harga: "",
    is_diskon: false,
    harga_diskon: "",
    category: "",
    image_url: "",
    description: "",
  });

  const handleChange = (e) => {
    if (e.target.name === "name") {
      setInput({ ...input, name: e.target.value });
    } else if (e.target.name === "stock") {
      setInput({ ...input, stock: e.target.value });
    } else if (e.target.name === "harga") {
        setInput({ ...input, harga: e.target.value });
    } else if (e.target.name === "is_diskon") {
        setInput({ ...input, is_diskon: e.target.checked });
    } else if (e.target.name === "harga_diskon") {
        setInput({ ...input, harga_diskon: e.target.value });
    } else if (e.target.name === "category") {
        setInput({ ...input, category: e.target.value });
    } else if (e.target.name === "image_url") {
      setInput({ ...input, image_url: e.target.value });
    } else if (e.target.name === "description") {
      setInput({ ...input, description: e.target.value }); // checkbox
    }
  };

  const resetForm = () => {
    setInput({
      name: "",
      stock: "",
      harga: "",
      is_diskon: false,
      harga_diskon: "",
      category: "",
      image_url: "",
      description: "",
    });
  };

  const fetchArticleById = async () => {
    try {
      // fetch data
      const response = await axios.get(
        `https://api-project.amandemy.co.id/api/products/${id}`
      );
      console.log(response.data.data);
      const article = response.data.data;
      // melakukan binding data dari server
      setInput({
        name: article.name,
        stock : article.stock,
        harga : article.harga,
        is_diskon : article.is_diskon,
        haga_diskon : article.harga_diskon,
        category: article.category,
        image_url: article.image_url,
        description: article.description,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const updateArticle = async () => {
    try {
      const response = await axios.put(
        `https://api-project.amandemy.co.id/api/products/${id}`,
        {
          name: input.name,
          stock: input.stock,
          harga: input.harga,
          is_diskon: input.is_diskon,
          harga_diskon: input.harga_diskon,
          category: input.category,
          image_url: input.image_url,
          description: input.description,
        }
      );
      alert("Berhasil mengupdate articles");
      // navigasi ke table
      navigate("/table");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  useEffect(() => {
    console.log(`Fetch Article id ${id}`);
    fetchArticleById();
  }, []);

  return (
    <div>
      <section className="max-w-xl mx-auto border-2 border-gray-600 p-6 mt-12">
        <h1 className="text-center text-2xl">
          Form Update Articles dengan ID {id}
        </h1>
        <form className="flex flex-col items-center">
          <div className="my-4">
            <label htmlFor="">Nama:</label>
            <input
              type="text"
              placeholder="Masukkan nama"
              className="pl-2 py-1 ml-10 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              name="name"
              value={input.name}
            />
          </div>
          <div className="my-4">
            <label htmlFor="">Stock:</label>
            <input
              type="text"
              placeholder="Masukkan Stock"
              className="pl-2 py-1 ml-10 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              name="stock"
              value={input.stock}
            />
          </div>
          <div className="my-4">
            <label htmlFor="">Harga:</label>
            <input
              type="number"
              placeholder="Masukkan Harga"
              className="pl-2 py-1 ml-4 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              name="harga"
              value={input.harga}
            />
          </div>
          <div className="my-4">
            <label htmlFor="">Harga Diskon:</label>
            <input
              type="Number"
              placeholder="Masukkan Harga Diskon"
              className="pl-2 py-1 ml-10 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              name="harga_diskon"
              value={input.harga_diskon}
            />
          </div>
          <div className="my-4">
            <label htmlFor="">Kategori:</label>
            <input
              type="text"
              placeholder="Masukkan Kategori"
              className="pl-2 py-1 ml-10 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              name="category"
              value={input.category}
            />
          </div>
          <div className="my-4">
            <label htmlFor="">Image URL:</label>
            <input
              type="text"
              placeholder="Masukkan image URL"
              className="pl-2 py-1 ml-4 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              name="image_url"
              value={input.image_url}
            />
          </div>
          <div className="my-4">
            <label htmlFor="">Deskripsi:</label>
            <input
              type="text"
              placeholder="Masukkan Deskripsi"
              className="pl-2 py-1 ml-4 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              name="description"
              value={input.description}
            />
          </div>
          <div className="my-4">
            <label htmlFor="">Is Highlight:</label>
            <input
              type="checkbox"
              className="ml-4 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              name="highlight"
              checked={input.highlight}
            />
          </div>
          <button
            type="button"
            onClick={updateArticle}
            className="px-6 py-2 mt-8 bg-blue-600 text-white rounded-md"
          >
            Update
          </button>
        </form>
      </section>
    </div>
  );
}

export default UpdateForm;


import React from "react";

function Article({ data }) {
  return (
    <div className="shadow-lg">
        <img
          src={data.image_url}
          className="w-full h-64 object-cover"
          alt=""
        />
        <div class="pl-4 my-4">
          <h1 className="text-blue-600 font-bold text-lg mb-3">{data.name}</h1>
          <p className="line-through text-red-600">{data.harga_display}</p>
          <p>{data.harga_diskon_display}</p>
          <p>Stock : {data.stock}</p>
        </div>
    </div>  
  );
}

export default Article;


import React, { useContext, useEffect } from "react";
import Article from "../components/Article";
import Navbar from "../components/Navbar";
import { ArticleContext } from "../context/ArticleContext";

function ListArticlePage() {
  const { articles, fetchArticles, loading, status } =
    useContext(ArticleContext);

  useEffect(() => {
    fetchArticles();
  }, []);

  return (
    <div>
      <Navbar />
      <section className="mx-32 mt-5 mb-5">

        <div className="flex justify-between mb-3">
          <h1>Catalog Products</h1>
          <button className="bg-blue-500 text-white px-4 py-2">See more</button>
        </div>

      {loading === false ? ( 
        <div className="grid grid-cols-4 gap-8">
          {articles.map((item, index) => {
            return <Article data={item} />;
          })}
        </div>
        ) : (
        <h1 className="text-center my-6 text-3xl font-bold">Loading ....</h1>
        )}
      </section>
    </div>
  );
}

export default ListArticlePage;


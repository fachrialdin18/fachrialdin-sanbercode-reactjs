import React from "react";
import Navbar from "../components/Navbar";
import UpdateForm from "../components/UpdateForm";

function UpdateArticlePage() {
  return (
    <div>
      <Navbar />
      <UpdateForm />
    </div>
  );
}

export default UpdateArticlePage;


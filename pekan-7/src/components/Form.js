import axios from "axios";
import React, { useContext, useState } from "react";
import { useFormik } from "formik"; 
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { ArticleContext } from "../context/ArticleContext";

const rulesSchema = Yup.object({
  name: Yup.string().required("Nama wajib diisi"),
  stock: Yup.number().required("Stock wajib diisi"),
  harga: Yup.number().required("Harga wajib diisi"),
  harga_diskon: Yup.number().required("Harga Diskon wajib diisi"),
  category: Yup.string().required("Category wajib diisi"),
  image_url: Yup.string()
    .required("Password wajib diisi")
    .url("Link Gambar tidak valid"),
});

function Form() {
  const { fetchArticles } = useContext(ArticleContext);
  const initialState = {
    name: "",
    stock: "",
    harga: "",
    is_diskon: false,
    harga_diskon: "",
    category: "",
    image_url: "",
    description: "",
  };

  const navigate = useNavigate();

  const storeArticle = async (values) => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/products",
        {
          name: values.name,
          stock: values.stock,
          harga: values.harga,
          is_diskon: values.is_diskon,
          harga_diskon: values.harga_diskon,
          category: values.category,
          image_url: values.image_url,
          description: values.description,
        }
      );
      alert("Berhasil Mengirim Request");
      // memannggil data kembali
      fetchArticles();
      resetForm();
      // navigasi ke halaman table
      navigate("/table");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  const {
    handleChange,
    handleSubmit,
    errors,
    handleBlur,
    touched,
    resetForm,
    values,
  } = useFormik({
      initialValues: initialState,
      onSubmit: storeArticle,
      validationSchema: rulesSchema,
  });



  return (
    <div className="form">
      <h1 className="text-2xl font-bold text-center mb-6">Form Card</h1>
      <form>
          <div className="form-container-2a">
              <div className="form-section">
                  <label for="nama-barang">Nama Barang</label>
                  <input onChange={handleChange} onBlur={handleBlur} value={values.name} type="text" name="name" placeholder="Masukkan Nama Barang" required />
                  <p className="text-red-600">
                    {touched.name === true && errors.name}
                  </p>
              </div>
              <div className="form-section">
                  <label for="stock-barang">Stock Barang</label>
                  <input onChange={handleChange} onBlur={handleBlur} value={values.stock} type="number" name="stock" placeholder="Masukkan Stock Barang" required />
                  <p className="text-red-600">
                    {touched.stock === true && errors.stock}
                  </p>
              </div>
          </div>
          <div className="form-container-3">
              <div className="form-section">
                  <label for="harga-barang">Harga Barang</label>
                  <input onChange={handleChange} onBlur={handleBlur} value={values.harga} type="number" step="0.01" name="harga" placeholder="Masukkan Harga Barang" required />
                  <p className="text-red-600">
                    {touched.harga === true && errors.harga}
                  </p>
              </div>
              <div className="form-section">
                  <label for="status-diskon">Status Diskon</label>
                  <input onChange={handleChange} onBlur={handleBlur} value={values.is_diskon} type="checkbox" name="is_diskon" />
              </div>
              <div className="form-section">
                  <label for="harga-diskon">Harga Diskon</label>
                  <input onChange={handleChange} onBlur={handleBlur} value={values.harga_diskon} type="number" name="harga_diskon" placeholder="Masukkan Harga Diskon" required />
                  <p className="text-red-600">
                    {touched.harga_diskon === true && errors.harga_diskon}
                  </p>
              </div>
          </div>
          <div className="form-container-2b">
              <div className="form-section">
                  <label for="jenis-barang">Kategori Barang</label>
                  <select onChange={handleChange} onBlur={handleBlur} value={values.category} name="category" className="dropdown">
                      <option value="">Silahkan Pilih Kategori</option>
                      <option value="teknologi">Teknologi</option>
                      <option value="makanan">Makanan</option>
                      <option value="minuman">Minuman</option>
                      <option value="hiburan">Hiburan</option>
                      <option value="kendaraan">Kendaraan</option>
                  </select>
                  <p className="text-red-600">
                    {touched.category === true && errors.category}
                  </p>
              </div>
              <div className="form-section">
                  <label for="image-barang">Image Barang</label>
                  <input onChange={handleChange} onBlur={handleBlur} value={values.image_url} type="text" name="image_url" placeholder="Masukkan Image Barang" required />
                  <p className="text-red-600">
                    {touched.image_url === true && errors.image_url}
                  </p>
              </div>
          </div>
          <div className="form-container-1">
              <div className="form-section">
                  <label for="Deskripsi">Deskripsi (Optional)</label>
                  <textarea onChange={handleChange} onBlur={handleBlur} value={values.description} name="description" placeholder="Masukkan Deskripsi Barang" required  />
              </div>
          </div>
          <button onClick={handleSubmit} className="bg-green-300 border px-6 py-2 mt-5">Create</button>
      </form>
    </div>
  );
}

export default Form;

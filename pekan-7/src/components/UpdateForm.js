import axios from "axios";
import React, { useEffect, useState } from "react";
import { useFormik } from "formik";
import { useNavigate, useParams } from "react-router-dom";
import * as Yup from "yup";

const rulesSchema = Yup.object({
  name: Yup.string().required("Nama wajib diisi"),
  stock: Yup.number().required("Stock wajib diisi"),
  harga: Yup.number().required("Harga wajib diisi"),
  harga_diskon: Yup.number().required("Harga Diskon wajib diisi"),
  category: Yup.string().required("Category wajib diisi"),
  image_url: Yup.string()
    .required("Password wajib diisi")
    .url("Link Gambar tidak valid"),
});

function UpdateForm() {
  const { id } = useParams();
  const navigate = useNavigate();

  const initialState = {
    name: "",
    stock: "",
    harga: "",
    is_diskon: false,
    harga_diskon: "",
    category: "",
    image_url: "",
    description: "",
  };

  const [input, setInput] = useState(initialState);

  const fetchArticleById = async () => {
    try {
      // fetch data
      const response = await axios.get(
        `https://api-project.amandemy.co.id/api/products/${id}`
      );
      console.log(response.data.data);
      const article = response.data.data;
      // melakukan binding data dari server
      setInput({
        name: article.name,
        stock : article.stock,
        harga : article.harga,
        is_diskon : article.is_diskon,
        harga_diskon : article.harga_diskon,
        category: article.category,
        image_url: article.image_url,
        description: article.description,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const updateArticle = async (values) => {
    try {
      const response = await axios.put(
        `https://api-project.amandemy.co.id/api/products/${id}`,
        {
          name: values.name,
          stock: values.stock,
          harga: values.harga,
          is_diskon: values.is_diskon,
          harga_diskon: values.harga_diskon,
          category: values.category,
          image_url: values.image_url,
          description: values.description,
        }
      );
      alert("Berhasil mengupdate articles");
      // navigasi ke table
      navigate("/table");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  useEffect(() => {
    console.log(`Fetch Article id ${id}`);
    fetchArticleById();
  }, []);

  const {
    handleChange,
    handleSubmit,
    errors,
    handleBlur,
    touched,
    resetForm,
    values,
  } = useFormik({
      initialValues: input,
      enableReinitialize: true,
      onSubmit: updateArticle,
      validationSchema: rulesSchema,
  });


  return (
    <div>
      <section className="max-w-xl mx-auto border-2 border-gray-600 p-6 mt-12">
        <h1 className="text-center text-2xl">
          Form Update Articles dengan ID {id}
        </h1>
        <form className="flex flex-col items-center">
          <div className="my-4">
            <label htmlFor="">Nama:</label>
            <input
              type="text"
              placeholder="Masukkan nama"
              className="pl-2 py-1 ml-10 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              onBlur={handleBlur}
              name="name"
              value={values.name}
            />
            <p className="text-red-600">
              {touched.name === true && errors.name}
            </p>
          </div>
          <div className="my-4">
            <label htmlFor="">Stock:</label>
            <input
              type="text"
              placeholder="Masukkan Stock"
              className="pl-2 py-1 ml-10 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              onBlur={handleBlur}
              name="stock"
              value={values.stock}
            />
            <p className="text-red-600">
              {touched.stock === true && errors.stock}
            </p>
          </div>
          <div className="my-4">
            <label htmlFor="">Harga:</label>
            <input
              type="number"
              placeholder="Masukkan Harga"
              className="pl-2 py-1 ml-4 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              onBlur={handleBlur}
              name="harga"
              value={values.harga}
            />
            <p className="text-red-600">
              {touched.harga === true && errors.harga}
            </p>
          </div>
          <div className="my-4">
            <label htmlFor="">Harga Diskon:</label>
            <input
              type="Number"
              placeholder="Masukkan Harga Diskon"
              className="pl-2 py-1 ml-10 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              onBlur={handleBlur}
              name="harga_diskon"
              value={values.harga_diskon}
            />
            <p className="text-red-600">
              {touched.harga_diskon === true && errors.harga_diskon}
            </p>
          </div>
          <div className="my-4">
            <label htmlFor="">Kategori:</label>
            <input
              type="text"
              placeholder="Masukkan Kategori"
              className="pl-2 py-1 ml-10 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              onBlur={handleBlur}
              name="category"
              value={values.category}
            />
            <p className="text-red-600">
              {touched.category === true && errors.category}
            </p>
          </div>
          <div className="my-4">
            <label htmlFor="">Image URL:</label>
            <input
              type="text"
              placeholder="Masukkan image URL"
              className="pl-2 py-1 ml-4 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              onBlur={handleBlur}
              name="image_url"
              value={values.image_url}
            />
            <p className="text-red-600">
              {touched.image_url === true && errors.image_url}
            </p>
          </div>
          <div className="my-4">
            <label htmlFor="">Deskripsi:</label>
            <input
              type="text"
              placeholder="Masukkan Deskripsi"
              className="pl-2 py-1 ml-4 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              onBlur={handleBlur}
              name="description"
              value={values.description}
            />
            <p className="text-red-600">
              {touched.description === true && errors.description}
            </p>
          </div>
          <div className="my-4">
            <label htmlFor="">Is Highlight:</label>
            <input
              type="checkbox"
              className="ml-4 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              onBlur={handleBlur}
              name="highlight"
              checked={values.highlight}
            />
          </div>
          <button
            type="button"
            onClick={handleSubmit}
            className="px-6 py-2 mt-8 bg-blue-600 text-white rounded-md"
          >
            Update
          </button>
        </form>
      </section>
    </div>
  );
}

export default UpdateForm;


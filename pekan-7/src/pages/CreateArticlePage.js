import React from "react";
import Form from "../components/Form";
import Navbar from "../components/Navbar";
import { Helmet } from "react-helmet";


function CreateArticlePage() {
  return (
    <div>
      <Helmet>
        <title>Form Page</title>
      </Helmet>
      <Navbar />
      <Form />
    </div>
  );
}

export default CreateArticlePage;


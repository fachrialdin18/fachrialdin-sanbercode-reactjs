import React, { useContext, useEffect } from "react";
import Navbar from "../components/Navbar";
import TableProduct from "../components/TableProduct";
import { ArticleContext } from "../context/ArticleContext";
import { Helmet } from "react-helmet";

function TableArticlePage() {
  const { fetchArticles } = useContext(ArticleContext);
  useEffect(() => {
    fetchArticles();
  }, []);
  return (
    <div>
      <Helmet>
        <title>Table Product</title>
      </Helmet>
      <Navbar />
      <TableProduct />
    </div>
  );
}

export default TableArticlePage;


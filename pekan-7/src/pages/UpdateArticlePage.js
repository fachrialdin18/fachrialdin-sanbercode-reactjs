import React from "react";
import Navbar from "../components/Navbar";
import UpdateForm from "../components/UpdateForm";
import { Helmet } from "react-helmet";

function UpdateArticlePage() {
  return (
    <div>
      <Helmet>
        Update Product
      </Helmet>
      <Navbar />
      <UpdateForm />
    </div>
  );
}

export default UpdateArticlePage;


import React from 'react'

const Card = () => {
  return (
    <div>
      <h1 className='mb-4 text-4xl font-extrabold leading-none tracking-tight text-gray-900 md:text-5xl lg:text-6xl dark:text-black'>Halo Dunia</h1>
      <p>Semoga hari-harimu menyenangkan</p>
    </div>
  )
}

export default Card

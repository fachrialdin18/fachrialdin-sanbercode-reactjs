import axios from "axios";
import React, { useContext, useState } from "react";
import { ArticleContext } from "../context/ArticleContext";

function Form() {
  const { fetchArticles } = useContext(ArticleContext);
  const [input, setInput] = useState({
    name: "",
    content: "",
    image_url: "",
    highlight: false,
  });

  const handleChange = (e) => {
    if (e.target.name === "name") {
      setInput({ ...input, name: e.target.value });
    } else if (e.target.name === "content") {
      setInput({ ...input, content: e.target.value });
    } else if (e.target.name === "image_url") {
      setInput({ ...input, image_url: e.target.value });
    } else if (e.target.name === "highlight") {
      setInput({ ...input, highlight: e.target.checked });
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(input);
  };

  const resetForm = () => {
    setInput({
      name: "",
      content: "",
      image_url: "",
      highlight: false,
    });
  };

  const storeArticle = async () => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/articles",
        {
          name: input.name,
          image_url: input.image_url,
          highlight: input.highlight,
          content: input.content,
        }
      );
      alert("Berhasil Mengirim Request");
      // memannggil data kembali
      fetchArticles();
      resetForm();
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  return (
    <div>
      <section className="max-w-xl mx-auto border-2 border-gray-600 p-6 mt-12">
        <h1 className="text-center text-2xl">Form Create Articles</h1>
        <form className="flex flex-col items-center">
          <div className="my-4">
            <label htmlFor="">Nama:</label>
            <input
              type="text"
              placeholder="Masukkan nama"
              className="pl-2 py-1 ml-10 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              name="name"
              value={input.name}
            />
          </div>
          <div className="my-4">
            <label htmlFor="">Konten:</label>
            <input
              type="text"
              placeholder="Masukkan Konten"
              className="pl-2 py-1 ml-10 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              name="content"
              value={input.content}
            />
          </div>
          <div className="my-4">
            <label htmlFor="">Image URL:</label>
            <input
              type="text"
              placeholder="Masukkan image URL"
              className="pl-2 py-1 ml-4 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              name="image_url"
              value={input.image_url}
            />
          </div>
          <div className="my-4">
            <label htmlFor="">Is Highlight:</label>
            <input
              type="checkbox"
              className="ml-4 border-2 border-gray-600 rounded-md"
              onChange={handleChange}
              name="highlight"
              checked={input.highlight}
            />
          </div>
          <button
            type="button"
            onClick={storeArticle}
            className="px-6 py-2 mt-8 bg-blue-600 text-white rounded-md"
          >
            Submit
          </button>
        </form>
      </section>
    </div>
  );
}

export default Form;

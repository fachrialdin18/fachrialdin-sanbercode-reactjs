import axios from "axios";
import React, { useContext } from "react";
import { ArticleContext } from "../context/ArticleContext";

function TableProduct({ setEditArticle }) {
  const { articles, fetchArticles } = useContext(ArticleContext);

  const onUpdate = (article) => {
    // alert(id);
    setEditArticle(article);
  };
  const onDelete = async (id) => {
    // alert(`Berhasil Menghapus Artikel dengan ID: ${id}`);
    try {
      // fetch data
      const response = await axios.delete(
        `https://api-project.amandemy.co.id/api/articles/${id}`
      );
      alert("Berhasil Menghapus Product");
      fetchArticles();
    } catch (error) {
      console.log(error);
      alert("Gagal Menghapus Product");
    }
  };
  return (
    <section>
      <h1 className="my-8 text-3xl font-bold text-center">Table Article</h1>
      <div className="max-w-4xl mx-auto w-full my-4">
        <table className="border border-gray-500 w-full">
          <thead>
            <tr>
              <th className="border border-gray-500 p-2">ID</th>
              <th className="border border-gray-500 p-2">Name</th>
              <th className="border border-gray-500 p-2">Content</th>
              <th className="border border-gray-500 p-2">Image</th>
              <th className="border border-gray-500 p-2">Highlight</th>
              <th className="border border-gray-500 p-2">Action</th>
            </tr>
          </thead>
          <tbody>
            {articles.map((item, index) => {
              return (
                <tr>
                  <td className="border border-gray-500 p-2">{item.id}</td>
                  <td className="border border-gray-500 p-2">{item.name}</td>
                  <td className="border border-gray-500 p-2">{item.content}</td>
                  <td className="border border-gray-500 p-2">
                    <img src={item.image_url} alt="" className="w-64" />
                  </td>
                  <td className="border border-gray-500 p-2">
                    {item.highlight === true ? "Aktif" : "Tidak Aktif"}
                  </td>

                  <td className="border border-gray-500 p-2">
                    <div className="flex gap-2">
                      <button
                        onClick={() => onUpdate(item)}
                        className="px-3 py-1 bg-yellow-600 text-white"
                      >
                        Update
                      </button>
                      <button
                        onClick={() => onDelete(item.id)}
                        className="px-3 py-1 bg-red-600 text-white"
                      >
                        Delete
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </section>
  );
}

export default TableProduct;


import React, { useContext, useEffect } from "react";
import Navbar from "../components/Navbar";
import TableProduct from "../components/TableProduct";
import { ArticleContext } from "../context/ArticleContext";

function TableArticlePage() {
  const { fetchArticles } = useContext(ArticleContext);
  useEffect(() => {
    fetchArticles();
  }, []);
  return (
    <div>
      <Navbar />
      <TableProduct />
    </div>
  );
}

export default TableArticlePage;

